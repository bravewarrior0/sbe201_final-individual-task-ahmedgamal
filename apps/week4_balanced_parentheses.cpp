#include <iostream>
#include "stackArray.hpp"

int main( int argc , char **argv )
{
    int i = 0;
    stack::CharacterStack100 buffer;
        
    while( argv[1][i] != '\0' )
    {
        if( argv[1][i] == '(' )     buffer.push( '(' );
        
        else if( argv[1][i] == ')' )
        {    
            if( buffer.isEmpty() )
            {
                std::cout << "not balanced\n";
                return 0;
            }
            
            else    buffer.pop();
        }
        
        ++i;
    }
    
    if( buffer.isEmpty() )  std::cout << "balanced\n";
    else    std::cout << "not balanced\n";
    
    return 0;
}
