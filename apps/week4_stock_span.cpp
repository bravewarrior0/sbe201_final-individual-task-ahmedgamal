#include <iostream>
#include "stock_span.hpp"

int main( int argc , char **argv )
{
    int size = 0;
    
    if( argc == 1 )
    {
        std::cout << "Enter the number of stock prices:\n";
        std::cin >> size;
        int *prices = new int[size];
        std::cout << "Enter the stock prices:\n";
        for( int i = 0; i < size; ++i)  std::cin >> prices[i];
        
        int *span = stock::span( prices , size );
    
        for( int i = 0; i <size; ++i)  std::cout << span[i] << " ";
        std::cout << std::endl;
        
        delete [] prices;
        delete [] span;
    }
    
    else if( argc == 2 )
    {
        int *prices = stock::getPrices( argv[1], size );
        
        int *span = stock::span( prices , size );
    
        for( int i = 0; i <size; ++i)  std::cout << span[i] << " ";
        std::cout << std::endl;
        
        delete [] prices;
        delete [] span;
    }
    
    else
    {
        size = argc - 1;
        int *prices = new int[size];
        for( int i = 0; i < size; ++i)  prices[i] = std::atoi( argv[ i+1 ] );
        
        int *span = stock::span( prices , size );
    
        for( int i = 0; i <size; ++i)  std::cout << span[i] << " ";
        std::cout << std::endl;
        
        delete [] prices;
        delete [] span;
    }

    return 0;
}
