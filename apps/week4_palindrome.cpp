#include <iostream>
#include "stackArray.hpp"
#include "queueArray.hpp"

int main()
{
    stack::CharacterStack100 text;
    queue::CharQueue text_without_spaces;
    char in[100];
    char out[100];

    std::cout << "Enter a text to check if it's a palindrome \n";
    std::cin.getline(in, sizeof(in));

    int i = 0;
    while (in[i] != '\0')
        i++;

    for (int x = 0; x < i; x++)
    {
        if (in[x] != ' ' && in[x] != '\'' && in[x] != '.' && in[x] != ',' && in[x] != '\"')
            enqueue(text_without_spaces, in[x]);
    }
  //  printQueue(text_without_spaces);
   //std:: cout<<text_without_spaces.back;

    i = text_without_spaces.back + 1;

    for (int x = 0; x < i; x++)
    {
        in[x] = queue::front(text_without_spaces);
        queue::dequeue(text_without_spaces);
      //  std::cout<<in[x];
    }
   // std::cout << in << "\n";
    for (int x = 0; x < i; x++)
        text.push(in[x]);

    for (int x = 0; x < i + 1; x++)
        out[x] = text.pop();

    bool flag = false;
   // std::cout << out << "\n";
    for (int x = 0; x < i ; x++)
    {
        if (in[x] == out[x])
            flag = true;
        else
        {
            flag = false;
            break;
        }
    }
    if (flag == true)
        std::cout << "nice palindrome \n";
    else
        std::cout << "sorry not a palindrome \n";
        return 0;
}
