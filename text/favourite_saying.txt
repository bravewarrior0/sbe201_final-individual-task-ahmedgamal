"Lost time is never found again."Ben Franklin 

"Mistakes are a part of being human. Appreciate your mistakes for what they are: precious life lessons that can only be learned the hard way."   Al Franken
