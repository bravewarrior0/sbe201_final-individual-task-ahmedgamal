cmake_minimum_required(VERSION 3.5 )

### c++11 standards
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

project( DataStructure_work_year )

include_directories(include)

### Generate executables
add_executable( week1_Basics apps/week1_basics.cpp )
add_executable( week2_Memory apps/week2_memory.cpp )
add_executable( week3_AnalyzeDNA apps/week3_analyzeDNA.cpp )
add_executable( week3_AnalyzeECG apps/week3_analyzeECG.cpp )
add_executable( week3_Calculator apps/week3_calculator.cpp )
add_executable( week3_Heron apps/week3_heron.cpp )
add_executable( week4_AwesomeApplication apps/week4_awesome_application.cpp )
add_executable( week4_BalancedParentheses apps/week4_balanced_parentheses.cpp )
add_executable( week4_Palindrome apps/week4_palindrome.cpp )
add_executable( week4_Stock_span apps/week4_stock_span.cpp )
add_executable( week8_CountDNA apps/week8_countDNA.cpp )
add_executable( week8_Count_words apps/week8_count_words.cpp )
add_executable( week8_Test_tree apps/week8_test_tree.cpp )
add_executable( week8_Unique_words apps/week8_unique_words.cpp )




