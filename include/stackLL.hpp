/*

 * Requirement 1: (Deadline Sunday 2:00 AM)
    - Implement stacks of char and int using linked list
      Supported Operations:
        `push`.
        `pop`.
        `size`.
        is empty? `isEmpty`.
        return front `front` (LIFO).
        delete the whole stack `clear`.
    - Make sure to follow the rules of Bonus Grades

 * Requirement 2:
    - Cooperate with Ahmed, Sohail, Osama and Nasr to finish A3 (Ahmed is the team leader)
    - Make sure to follow the rules of Bonus Grades

*/


#ifndef MEMBER3_HPP
#define MEMBER3_HPP
#include "List.hpp"



namespace stack
{
//Characters:
struct CharStackLL 
{
    bool isEmpty( list::CharLL &stack )
        {
            return stack.front == nullptr;
        }

    void push( list::CharLL &stack , char data )
        {
            list::insertFront( stack, data );
        }
	
    char pop ( list::CharLL &stack )
        {
            if( !isEmpty(stack) )
            {
                char lifo = list::front( stack );
                list::removeFront( stack );
                return lifo;
            }
            else
            {
                exit( 1 );
            }
        }

    char front( list::CharLL &stack )
        {
            return stack.front->data;
        }

    void clear( list::CharLL &stack )
        {
            list::clear( stack );
        }

    int size( list::CharLL &stack )
        {
            list::size( stack );
        }

};

//Integers:
struct IntStackLL
{
    bool isEmpty( list::IntegerLL &stack )
        {
            return stack.front == nullptr;
        }

    void push( list::IntegerLL &stack , int data )
        {
            list::insertFront( stack, data );
        }
	
    int pop ( list::IntegerLL &stack )
        {
            if( !isEmpty(stack) )
            {
                int lifo = list::front( stack );
                list::removeFront( stack );
                return lifo;
            }
            else
            {
                exit( 1 );
            }
        }

    int front( list::IntegerLL &stack )
        {
            return stack.front->data;
        }

    void clear( list::IntegerLL &stack )
        {
            list::clear( stack );
        }

    int size( list::IntegerLL &stack )
        {
            list::size( stack );
        }
};
}

#endif // MEMBER3_HPP
