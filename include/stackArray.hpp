/*
 * Requirement 1: (Deadline Saturday 10:00 AM)
    - Implement stacks of 'char' & 'int' using arrays and support the operations stated in Assignment 4
    - Make sure to follow the rules of Bonus Grades
    
 * Requirement 2:
    - Based on your resume, you have an experience in OOP
    - You are required to make sure that every header file code follow basic OOP rules stated in week 4 notes Part 2
        - You are required to create a new branch with name "OPP" to implement your edits
        - Watch videos 8 & 9 in this Youtube playlist to learn how: https://www.youtube.com/playlist?list=PL4cUxeGkcC9goXbgTDQ0n_4TBzOO0ocPR
        - You can ask Sohail for more information or help
*/
#ifndef MEMBER6_HPP
#define MEMBER6_HPP

namespace stack
{
    struct IntegerStack100
    {
        int buffer[ 100 ];
        int top = -1;
    
        void push( int newElement )
        {
            ++this->top;
            this->buffer[ this->top ] = newElement;
        }
        
        int pop()
        {
            int lifo = this->buffer[ this->top ];
            --this->top;
            return lifo;
        }

        int size()
        {
            return ( this->top + 1 ); 
        }
        
        bool isEmpty()
        {
            return ( this->top == -1 );
        }
        
        int front()
        {
            return this->buffer[this->top];
        }
        
        void clear()
        {
            this->top= -1;  
        }
    };


    struct CharacterStack100
    {
        char buffer[ 100 ];
        int top = -1;
        
        void push( char newElement )
        {
            ++this->top;
            this->buffer[ this->top ] = newElement;
        }
        
        int pop()
        {
            char lifo = this->buffer[ this->top ];
            --this->top;
            return lifo;
        }

        int size()
        {
            return ( this->top + 1 ); 
        }
        
        bool isEmpty()
        {
            return ( this->top == -1 );
        }
        
        int front()
        {
            return this->buffer[this->top];
        }
        
        void clear()
        {
            this->top = -1;
        }
    };
    
}

#endif // MEMBER6_HPP
