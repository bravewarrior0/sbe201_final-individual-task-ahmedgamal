#ifndef DNA_HPP
#define DNA_HPP

#include "arrays.hpp"
namespace dna
{
using DNArray = arrays::CharacterArray;
struct countDNA
{
    int countA;
    int countT;
    int countG;
    int countC;
};
struct AnalyzeDNA
{
    countDNA counter;
    char *DNA;
};
char complementaryBase(char base)
{
    switch (base)
    {
    case 'A':
    {
        return 'T';
    }
    break;

    case 'T':
    {
        return 'A';
    }
    break;

    case 'C':
    {
        return 'G';
    }
    break;

    default:
    {
        return 'C';
    }
    break;
    }
}
char *complementarySequence(char *base, int size)
{
    char *complementarySeq = new char[size];
    int i = 0, x = size - 1; // i for countup, xfor count down to complement
    for (int i = 0; i < size; ++i)
    {
        complementarySeq[x] = complementaryBase(base[i]);
        x--;
    }
    return &complementarySeq[0];
}
char *complementarySequence(DNArray DNA)
{
    return complementarySequence(DNA.base, DNA.size);
}
/*char *analyzeDNA(char *base, int size, int &countA, int &countC, int &countG, int &countT)
{
    // I count characters in dna Array not in complementarySequence as I understand
    // IF you would like to count complementarySequence'char just call count with
    //complementarySequence(base, size) insted of base
    countA = arrays::countCharacter(base, size, 'A');
    countC = arrays::countCharacter(base, size, 'C');
    countG = arrays::countCharacter(base, size, 'G');
    countT = arrays::countCharacter(base, size, 'T');
    return (complementarySequence(base, size));
}*/
AnalyzeDNA analyzeDNA(DNArray DNA)
{
    AnalyzeDNA analyze;
    analyze.counter.countA = arrays::countCharacter(DNA, 'A');
    analyze.counter.countC = arrays::countCharacter(DNA, 'C');
    analyze.counter.countG = arrays::countCharacter(DNA, 'G');
    analyze.counter.countT = arrays::countCharacter(DNA, 'T');
    analyze.DNA = complementarySequence(DNA);
    return analyze;
}
}

#endif // DNA_HPP
