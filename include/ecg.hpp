#ifndef ECG_HPP
#define ECG_HPP

#include "arrays.hpp"

namespace ecg
{
using ECGArray = arrays::DoubleArray;
struct Statistics
{
    double mean;
    double variance;
    double min;
    double max;
};

/*void analyzeECG(double *base, int arraySize, double &mean, double &variance, double &max, double &min)
{
    mean = arrays::meanArray(base, arraySize);
    max = arrays::maxArray(base, arraySize);
    min = arrays::minArray(base, arraySize);
    variance = arrays::varianceArray(base, arraySize);
}*/
/* i didn't like to use void analyzeECG as it is because I couldn't
benfit from the struct and if I modified it. looks like repeating my self so
I refered not to use it and make Statistics analyzeECG 6 lines */
Statistics analyzeECG(ECGArray ecg)
{
    Statistics statistics;
    statistics.mean = arrays::meanArray(ecg);
    statistics.max = arrays::maxArray(ecg);
    statistics.min = arrays::minArray(ecg);
    statistics.variance = arrays::varianceArray(ecg);
    return statistics;
}
}
#endif // ECG_HPP
