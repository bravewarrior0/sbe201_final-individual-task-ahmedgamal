/*
 * Requirement 1: (Deadline Sunday 10:00 PM)
    - Implement queues of char and double using linked lists..
        Supported Operations:
            `enqueue`.
            `dequeue`.
            `size`.
            `isEmpty`.
            return `front` (FIFO).
            delete the whole queue `clear`.
    - Make sure to follow the rules of Bonus Grades

 * Requirement 2:
    - Cooperate with Ahmed, Osama, Motair and Sohail to finish A3 (Ahmed is the team leader)
    - Make sure to follow the rules of Bonus Grades

 ***** check readme ******
*/

#ifndef MEMBER5_HPP
#define MEMBER5_HPP
#include "List.hpp"

namespace queue
{
//****DOUBLE****

struct DoubleNode
{
    double data;
    DoubleNode *next;
};

struct DoubleQueueLL
{
    DoubleNode *front;
    DoubleNode *back;
};

//isEmpty
bool isEmpty(DoubleQueueLL queue)
{
    return (queue.back == nullptr);
}

//Enqueuing
void enqueue(DoubleQueueLL &queue, double newSample)
{
    if (isEmpty(queue))
    {
        queue.back = new DoubleNode{newSample, nullptr};
        queue.front = queue.back;
    }
    else
    {
        DoubleNode *oldBack = queue.back;
        DoubleNode *newBack = new DoubleNode{newSample, nullptr};
        queue.back = newBack;
        oldBack->next = newBack;
    }
}

//Dequeuing
double dequeue(DoubleQueueLL &queue)
{
    if (isEmpty(queue))
    {
        std::cout << "queue is empty";
    }
    else
    {
        DoubleNode *oldfront = queue.front;
        double fifo = queue.front->data;
        queue.front = queue.front->next;
        delete oldfront;
        return fifo;
    }
}

//size
double size(DoubleQueueLL &queue)
{
    int i = 0;
    DoubleNode *current = queue.front;
    while (current != nullptr)
    {
        ++i;
        current = current->next;
    }
    return i;
}

//return front
double front(DoubleQueueLL &queue)
{
    if (queue.front != nullptr)
    {
        return queue.front->data;
    }
    else
    {
        std::cout << "queue is empty";
    }
}

//clear
void clear(DoubleQueueLL &queue)
{
    DoubleNode *catcher;
    while (queue.front != nullptr)
    {
        catcher = queue.front->next;
        delete queue.front;
        queue.front = catcher;
    }
    queue.back = nullptr;
}

//****CHAR****
struct CharNode
{
    char data;
    CharNode *next ;
};

struct CharQueueLL
{
    CharNode *front;
    CharNode *back ;
};

//isEmpty
bool isEmpty(CharQueueLL queue)
{
    return (queue.back == nullptr);
}

//Enqueuing
void enqueue(CharQueueLL &queue, char newSample)
{
    if (isEmpty(queue))
    {
        queue.back = new CharNode{newSample, nullptr};
        queue.front = queue.back;
    }
    else
    {
        CharNode *oldBack = queue.back;
        CharNode *newBack = new CharNode{newSample, nullptr};
        queue.back = newBack;
        oldBack->next = newBack;
    }
}

//Dequeuing

void denqueue(CharQueueLL &queue)
{
    if (isEmpty(queue))
    {
        std::cout << "queue is empty";
    }
    else
    {
        CharNode *oldfront = queue.front;
        queue.front = queue.front->next;
        delete oldfront;
    }
}

//size

char size(CharQueueLL &queue)
{
    int i = 0;
    CharNode *current = queue.front;
    while (current != nullptr)
    {
        ++i;
        current = current->next;
    }
    return i;
}

//return front

char front(CharQueueLL &queue)
{
    if (queue.front != nullptr)

    {
        return queue.front->data;
    }
}

//clear

void clear(CharQueueLL &queue)
{
    CharNode *catcher;
    while (queue.front != nullptr)

    {
        catcher = queue.front->next;
        delete queue.front;
        queue.front = catcher;
    }

    queue.back = nullptr;
}
}

#endif // MEMBER5_HPP
