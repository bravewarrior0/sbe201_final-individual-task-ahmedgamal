/*
 * Requirement 1: (Deadline Sunday 10:00 AM)
    - Finish A1
    - Make sure to follow the rules of Bonus Grades
    
 * Requirement 2: (Deadline Sunday 11:59 PM)
    - Finish A2
    - Make sure to follow the rules of Bonus Grades
    
 * Requirement 3:
    - Cooperate with Ahmed, Osama, Motair and Nasr to finish A3 (Ahmed is the team leader)
    - Make sure to follow the rules of Bonus Grades
    
 * Requirement 4:
    - Fulfill KISS & DRY for all header files
*/

#ifndef MEMBER2_HPP
#define MEMBER2_HPP

#include <string>
#include <vector>
#include <fstream>
#include<iostream>
#include <algorithm>
#include "stackArray.hpp"

namespace stock
{
    int *span( int *prices , int size )
    {
        int *span = new int[size];
        //creating new stack to store the days of interest
        //days numbers start from zero like the array; price in day 0 == prices[0]
        stack::IntegerStack100 days;
        days.push( 0 );
        //the span of first day is always 1
        span[0] = 1;
        
        for ( int i = 1; i < size; ++i)
        {
            bool done = false;
            //this variable must be declared because when the stack is empty days.front() != -1
            int h = 0;
            
            while( !days.isEmpty() && !done)
            {
                if( prices[i] < prices[days.front()] )    done = true;
                else    days.pop();
            }
            
            if(days.isEmpty())    h = -1;
            else    h = days.front();
            
            //span of any day = day number(index) - the front of the stack (if not empty)
            span[i] = i - h;
            days.push( i );
        }
        
        return span;
    }
    
    //this function gets the stock prices from a txt file
    int *getPrices( const char *filePath, int &size )
    {
        std::ifstream file( filePath );
        std::vector< int > price;
        std::string line;
        if( file )
        {
            while( std::getline( file , line ))
            {
                price.push_back( std::stoi( line ));
            }
        }
        else 
        {
           std::cout << "Failed to open file:" << filePath << std::endl;
            exit(1);
        }

        int *buffer = new int[ price.size() ];
        std::copy( price.begin() , price.end() , &buffer[0] );
        size = price.size();
        return buffer;   
    }
}

#endif // MEMBER2_HPP
