//
// Created by asem on 01/04/18.
//

#ifndef SBE201_WORDCOUNT_MAPS_MAP_HPP
#define SBE201_WORDCOUNT_MAPS_MAP_HPP

#include <string>
#include <iostream>

namespace map
{
struct MapNode
{
    std::string key;
    int value;
    MapNode *left;
    MapNode *right;
};

using WordMap = MapNode *;

WordMap create()
{
    return nullptr;
}

bool isEmpty(WordMap wmap)
{
    return wmap == nullptr;
}

bool isLeaf(WordMap wmap)
{
    return wmap->left == nullptr && wmap->right == nullptr;
}

int size(WordMap wmap)
{
    if (!isEmpty(wmap))
        return 1 + size(wmap->left) + size(wmap->right);

    else
        return 0;
}

bool find(WordMap wmap, std::string key)
{
    if (isEmpty(wmap))
        return false;
    else
    {
        int comparison = key.compare(wmap->key);
        if (comparison == 0)
            return true;
        else if (comparison < 0)
            return find(wmap->left, key);
        else
            return find(wmap->right, key);
    }
}

int &at(WordMap wmap, std::string key)
{
    if (isEmpty(wmap))
    {
        std::cout << "Key not found!" << std::endl;
        exit(1);
    }
    else
    {
        // COMPLETE HERE
        if (key.compare(wmap->key) == 0)
        {
          //  ++(wmap->value);
            return wmap->value;
        }
        else if (key.compare(wmap->key) < 0)
            return at(wmap->left, key);
        else
            return at(wmap->right, key);
        // DONE HERE
    }
}

void insert(WordMap &wmap, std::string key)
{
    if (isEmpty(wmap))
    {
        wmap = new MapNode;
        wmap->left = nullptr;
        wmap->right = nullptr;
        wmap->key = key;
        wmap->value = 0;
    }
    else if (key.compare(wmap->key) != 0) // Ignore when the key is already found
    {
        // COMLETE HERE
        if (key.compare(wmap->key) < 0)
            return insert(wmap->left, key);
        else
            return insert(wmap->right, key);
        // DONE HERE
    }
    // else ++(wmap->value);
}
WordMap minNode(WordMap wmap)
{
    if (wmap->left)
        return minNode(wmap->left);
    else
    {
        return wmap;
    }
}

void remove(WordMap &wmap, std::string key)
{
    if (isEmpty(wmap))
        return;

    if (key.compare(wmap->key) == 0)
    {
        if (!isEmpty(wmap->left) && !isEmpty(wmap->right))
        {
            WordMap minRight = minNode(wmap->right);
            wmap->key = minRight->key;
            wmap->value = minRight->value;
            remove(minRight, minRight->key);
        }
        else
        {
            WordMap discard = wmap;

            if (isLeaf(wmap))
                wmap = nullptr;
            else if (!isEmpty(wmap->left))
                wmap = wmap->left;
            else
                wmap = wmap->right;

            delete discard;
        }
    }
    else if (key.compare(wmap->key) < 0)
        return remove(wmap->left, key);
    else
        return remove(wmap->right, key);
}

int &value(WordMap &wmap, std::string key)
{
    if (!find(wmap, key))
    {
        insert(wmap, key);
    }
    return at(wmap, key);
}

void clear(WordMap &wmap)
{
    if (!isEmpty(wmap))
    {
        clear(wmap->left);
        clear(wmap->right);
        delete wmap;
        wmap = nullptr;
    }
}

void printAll(WordMap wmap)
{
    if (wmap)
    {
        printAll(wmap->left);
        std::cout << wmap->key << ":" << wmap->value << std::endl;
        printAll(wmap->right);
    }
}
}
#endif //SBE201_WORDCOUNT_MAPS_MAP_HPP
