//
// Created by asem on 01/04/18.
//

#ifndef SBE201_WORDCOUNT_MAPS_BST_HPP
#define SBE201_WORDCOUNT_MAPS_BST_HPP

#include <iostream>

#include <string>
#include <queue>

namespace bst
{

struct BSTNode
{
    std::string item;
    BSTNode *left;
    BSTNode *right;
};

using Tree = BSTNode *;

Tree create()
{
    return nullptr;
}

bool isEmpty(Tree node)
{
    return node == nullptr;
}

bool isLeaf(Tree node)
{
    return node->left == nullptr && node->right == nullptr;
}

int size(Tree node)
{
    if (!isEmpty(node))
        return 1 + size(node->left) + size(node->right);
    else
        return 0;
}

bool find(Tree tree, std::string item)
{
    if (isEmpty(tree))
        return false;
    else
    {
        if (item == tree->item)
            return true;

        else if (item < tree->item)
            return find(tree->left, item);

        else
            return find(tree->right, item);
    }
}

void insert(Tree &tree, std::string item)
{
    if (isEmpty(tree))
    {
        tree = new BSTNode;
        tree->item = item;
        tree->left = nullptr;
        tree->right = nullptr;
    }
    else
    {
        if (item.compare(tree->item) != 0)
        {
            if (item.compare(tree->item) < 0)
                insert(tree->left, item);
            else
                insert(tree->right, item);
        }
    }
}

Tree minNode(Tree tree)
{
    if (!isEmpty(tree->left))
        return minNode(tree->left);
    else
        return tree;
}

Tree maxNode(Tree tree)
{
    if (tree->right)
        return maxNode(tree->right);
    else
    {
        return tree;
    }
}

void remove(Tree &tree, std::string item)
{
    if (isEmpty(tree))
        return;

    if (item.compare(tree->item) == 0)
    {
        if (!isEmpty(tree->right) && !isEmpty(tree->left))
        {
            Tree minRight = minNode(tree->right);
            tree->item = minRight->item;
            remove(tree->right, minRight->item);
        }
        else
        {
            Tree discard = tree;

            if (isLeaf(tree))
                tree = nullptr;
            else if (!isEmpty(tree->left))
                tree = tree->left;
            else
                tree = tree->right;

            delete discard;
        }
    }
    else if (item.compare(tree->item) < 0)
        remove(tree->left, item);
    else
        remove(tree->right, item);
}

void clear(Tree &tree)
{
    if (!isEmpty(tree))
    {
        clear(tree->left);
        clear(tree->right);
        delete tree;
        tree = nullptr;
    }
}
void preorder(Tree tree)
{
    if (!isEmpty(tree))
    {
        std::cout << tree->item << '\n';
        preorder(tree->left);
        preorder(tree->right);
    }
}

void inorder(Tree tree)
{
    if (!isEmpty(tree))
    {
        preorder(tree->left);
        std::cout << tree->item << '\n';
        preorder(tree->right);
    }
}

void postorder(Tree tree)
{
    if (!isEmpty(tree))
    {
        preorder(tree->left);
        preorder(tree->right);
        std::cout << tree->item << '\n';
    }
}

void breadthFirst(Tree tree)
{
    if (isEmpty(tree))
        return;
    std::queue<Tree> q;
    q.push(tree);
    while (!q.empty())
    {
        tree = q.front();
        q.pop();
        std::cout << tree->item << " ";
        if (tree->left != nullptr)
            q.push(tree->left);
        if (tree->right != nullptr)
            q.push(tree->right);
    }
    std::cout << std::endl;
}
}

#endif //SBE201_WORDCOUNT_MAPS_BST_HPP_HPP
