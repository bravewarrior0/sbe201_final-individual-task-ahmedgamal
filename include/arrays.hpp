#ifndef ARRAYS_HPP
#define ARRAYS_HPP

#include <iostream>
#include "mathematics.hpp"

namespace arrays
{
struct DoubleArray
{
    double *base;
    int size;
};
struct CharacterArray
{
    char *base;
    int size;
};
void printAll(double *base, int arraySize)
{
    for (int i = 0; i < arraySize; ++i)
    {
        std::cout << base[i] << ", ";
    }
    std::cout << std::endl;
}
void printAll(DoubleArray array)
{
    printAll(array.base, array.size);
}
double maxArray(double *base, int arraySize)
{
    double max = base[0];
    for (int i = 0; i < arraySize; ++i)
    {
        if (base[i] > max)
        {
            max = base[i];
        }
    }
    return max;
}
double maxArray(DoubleArray array)
{
    maxArray(array.base, array.size);
}
double minArray(double *base, int arraySize)
{
    double min = base[0];
    for (int i = 0; i < arraySize; ++i)
    {
        if (base[i] < min)
        {
            min = base[i];
        }
    }
    return min;
}
double minArray(DoubleArray array)
{
    minArray(array.base, array.size);
}
double meanArray(double *base, int arraySize)
{
    double sum = 0;
    for (int i = 0; i < arraySize; ++i)
    {
        sum = sum + base[i];
    }
    return sum / arraySize;
}
double meanArray(DoubleArray array)
{
    meanArray(array.base, array.size);
}
double varianceArray(double *base, int arraySize)
{
    double sum = 0;
    for (int i = 0; i < arraySize; ++i)
    {
        sum = sum + mathematics::square(meanArray(base, arraySize) - base[i]);
    }
    return sum / arraySize;
}
double varianceArray(DoubleArray array)
{
    varianceArray(array.base, array.size);
}
int countCharacter(char *basePointer, int size, char query)
{
    int sum = 0;
    for (int i = 0; i < size; ++i)
    {
        if (basePointer[i] == query)
        {
            sum += 1;
        }
    }
    return sum;
}
int countCharacter(CharacterArray DNA, char query)
{
    return countCharacter(DNA.base, DNA.size, query);
}
}

#endif // ARRAYS_HPP
