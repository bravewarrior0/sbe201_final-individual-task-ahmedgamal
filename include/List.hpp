/*
 * Requirement 1: (Deadline Saturday 9:00 AM)
    - Concrete Implementation Linked List of 'int', 'double', and 'char' **integrate it with Motair**
      Supported Operations:
        insertion at front `insertFront`.
        insertion at back `insertBack`.
        remove front `removeFront`.
        remove back `removeBack`.
        remove the kth-element `removeAt`.
        return front `front`.
        return back `back`.
        remove a node next to a given node `removeAfter`.
        get arbitrary kth-element `getAt`.
        remove nodes with given data `filter`.
        is empty `isEmpty`?
        size of the linked list `size`.
        printAll `printAll`.
        delete the whole list from the heap `clear`.
    - Make sure to follow the rules of Bonus Grades
    
 * Requirement 2:
    - Cooperate with Ahmed, Sohail, Motair and Nasr to finish A3 (Ahmed is the team leader)
    - Make sure to follow the rules of Bonus Grades
    
 * Requirement 3:
    - Fulfill Convention Names for all header files
*/
    
#ifndef MEMBER4_HPP
#define MEMBER4_HPP     
#include <iostream>

namespace list
{
//Characters:
	struct CharNode
	{
		char data;
		CharNode *next = nullptr;
	};

	struct CharLL
	{
		CharNode *front = nullptr;
	};

	//is empty `isEmpty`?
	bool isEmpty( const CharLL &list )
	{
		return ( list.front == nullptr );
	}

	//insertion at front `insertFront`.
	void insertFront( CharLL &list , char data )
	{
		CharNode *newNode = new CharNode{};
		newNode->data = data;
		newNode->next = list.front;
		list.front = newNode;
	}

	//insertion at back `insertBack`.
	void insertBack( CharLL &list, char data )
	{
		CharNode *current = list.front;
		if ( isEmpty(list) )
		{
			insertFront( list, data );
		}
		else
		{
			while ( current->next != nullptr )
			{
				current = current->next;
			}
			current->next = new CharNode{};
			current->next->data = data;
			current->next->next = nullptr;
		}
	}

	//remove front `removeFront`.
	void removeFront( CharLL &list )
	{
		if ( !isEmpty(list) )
		{
			CharNode *current = list.front;
			list.front = list.front->next;
			delete current;
		}
	}

	//remove back `removeBack`.
	void removeBack( CharLL &list )
	{
		if ( !isEmpty(list) ) 
		{
			CharNode *current = list.front;
			if ( list.front->next == nullptr )
			{
				list.front = nullptr;
				delete current;
			}
			else
			{
				CharNode *previous = nullptr;
				while ( current->next != nullptr )
				{
					previous = current;
					current = current->next;
				}
				previous->next = nullptr;
				delete current;
			}
		}
	}

	//return front `front`.
	char front( const CharLL &list )
	{
		if ( !isEmpty(list) )
		{
			return list.front->data;
		}
	}

	//return back `back`.
	char back( const CharLL &list )
	{
		if ( !isEmpty(list) )
		{
			CharNode *current = list.front;
			while (current->next != nullptr)
			{
				current = current->next;
			}
			return current->data;
		}
	}

	//get arbitrary kth-element `getAt`.
	char getAt( const CharLL &list, int index )
	{
		if ( (!isEmpty(list)) && (index > 0) )
		{
			CharNode *current = list.front;
			for ( int i = 1; i != index; ++i )
			{
				current = current->next;
			}
			return current->data;
		}
	}

	//remove the kth-element `removeAt`.
	void removeAt( CharLL &list, int index )
	{
		if ( !isEmpty(list) && (index > 0) )
		{
			if ( index == 1 )
			{
				removeFront( list );
			}
			else
			{
				CharNode *current = list.front->next;
				CharNode *previous = list.front;
				for ( int i = 2; i < index; ++i )
				{
					previous = current;
					current = current->next;
				}
				previous->next = current->next;
				delete current;
			}
		}
	}

	//remove a node next to a given node `removeAfter`.
	void removeAfter( CharLL &list, int index )
	{
		removeAt( list, index + 1 );
	}
	
	//remove nodes with given data `filter`.
	void filter( CharLL &list, char data )
	{
		while (list.front->data == data)
		{
			removeFront( list );
		}

		CharNode *current = list.front->next;
		CharNode *catcher = nullptr;
		int i = 2;  //counter will be used as index in 'removeAt' function 
		            //started counting from list.front->next
		while ( current != nullptr )
		{
			if ( current->data == data )
			{
				catcher = current->next;
				removeAt( list , i ); 
				current = catcher;
			}
			else
			{
				current = current->next;
			}
			++i;
		}
	}
	
	//size of the linked list `size`.
	int size( const CharLL &list )
	{
		int i = 0; //counter
		CharNode *current = list.front;
		while ( current != nullptr )
		{
			++i;
			current = current->next;
		}
		return i;
	}

	//printAll `printAll`.
	void printAll( const CharLL &list )
	{
		CharNode *current = list.front;
		while ( current != nullptr )
		{
			std::cout << current->data << " ";
			current = current->next;
		}
		std::cout << std::endl;
	}

	//delete the whole list from the heap `clear`.
	void clear( CharLL &list )
	{
		CharNode *current = list.front;
		CharNode *catcher;
		while ( current != nullptr )
		{
			catcher = current->next;
			delete current;
			current = catcher;
		}
	}

    
//Integers:
	struct IntegerNode
	{
		int data;
		IntegerNode *next = nullptr;
	};

	struct IntegerLL
	{
		IntegerNode *front = nullptr;
	};

	//is empty `isEmpty`?
	bool isEmpty( const IntegerLL &list )
	{
		return ( list.front == nullptr );
	}

	//insertion at front `insertFront`.
	void insertFront( IntegerLL &list, int data )
	{
		IntegerNode *newNode = new IntegerNode{};
		newNode->data = data;
		newNode->next = list.front;
		list.front = newNode;
	}

	//insertion at back `insertBack`.
	void insertBack( IntegerLL &list, int data )
	{
		IntegerNode *current = list.front;
		if ( isEmpty(list) )
		{
			insertFront( list, data );
		}
		else
		{
			while ( current->next != nullptr )
			{
				current = current->next;
			}
			current->next = new IntegerNode{};
			current->next->data = data;
			current->next->next = nullptr;
		}
	}

	//remove front `removeFront`.
	void removeFront( IntegerLL &list )
	{
		if ( !isEmpty(list) )
		{
			IntegerNode *current = list.front;
			list.front = list.front->next;
			delete current;
		}
	}

	//remove back `removeBack`.
	void removeBack( IntegerLL &list )
	{
		if ( !isEmpty(list) )
		{
			IntegerNode *current = list.front;
			if ( list.front->next == nullptr )
			{
				list.front = nullptr;
				delete current;
			}
			else
			{
				IntegerNode *previous = nullptr;
				while ( current->next != nullptr )
				{
					previous = current;
					current = current->next;
				}
				previous->next = nullptr;
				delete current;
			}
		}
	}

	//return front `front`.
	int front( const IntegerLL &list )
	{
		if ( !isEmpty(list) )
		{
			return list.front->data;
		}
	}

	//return back `back`.
	int back( const IntegerLL &list )
	{
		if ( !isEmpty(list) )
		{
			IntegerNode *current = list.front;
			while ( current->next != nullptr )
			{
				current = current->next;
			}
			return current->data;
		}
	}

	//get arbitrary kth-element `getAt`.
	int getAt( const IntegerLL &list , int index )
	{
		if ( (!isEmpty(list)) && (index > 0) )
		{
			IntegerNode *current = list.front;
			for ( int i = 1; i != index; ++i )
			{
				current = current->next;
			}
			return current->data;
		}
	}

	//remove the kth-element `removeAt`.
	void removeAt( IntegerLL &list, int index )
	{
		if ( !isEmpty(list) && (index > 0) )
		{
			if ( index == 1 )
			{
				removeFront( list );
			}
			else
			{
				IntegerNode *current = list.front->next;
				IntegerNode *previous = list.front;
				for ( int i = 2; i < index; ++i )
				{
					previous = current;
					current = current->next;
				}
				previous->next = current->next;
				delete current;
			}
		}
	}

	//remove a node next to a given node `removeAfter`.
	void removeAfter( IntegerLL &list, int index )
	{
		removeAt( list, index + 1 );
	}
	
	//remove nodes with given data `filter`.
	void filter( IntegerLL &list, int data )
	{
		while (list.front->data == data)
		{
			removeFront( list );
		}

		IntegerNode *current = list.front->next;
		IntegerNode *catcher = nullptr;
		int i = 2;  //counter will be used as index in 'removeAt' function 
		            //started counting from list.front->next
		while ( current != nullptr )
		{
			if ( current->data == data )
			{
				catcher = current->next;
				removeAt( list , i ); 
				current = catcher;
			}
			else
			{
				current = current->next;
			}
			++i;
		}
	}

	//size of the linked list `size`.
	int size( const IntegerLL &list )
	{
		int i = 0;  //counter
		IntegerNode *current = list.front;
		while ( current != nullptr )
		{
			++i;
			current = current->next;
		}
		return i;
	}

	//printAll `printAll`.
	void printAll( const IntegerLL &list )
	{
		IntegerNode *current = list.front;
		while ( current != nullptr )
		{
			std::cout << current->data << " ";
			current = current->next;
		}
		std::cout << std::endl;
	}

	//delete the whole list from the heap `clear`.
	void clear( IntegerLL &list )
	{
		IntegerNode *current = list.front;
		IntegerNode *catcher;
		while ( current != nullptr )
		{
			catcher = current->next;
			delete current;
			current = catcher;
		}
	}


//Double:
	struct DoubleNode
	{
		double data;
		DoubleNode *next = nullptr;
	};

	struct DoubleLL
	{
		DoubleNode *front = nullptr;
	};

	//is empty `isEmpty`?
	bool isEmpty( const DoubleLL &list )
	{
		return ( list.front == nullptr );
	}

	//insertion at front `insertFront`.
	void insertFront( DoubleLL &list, double data )
	{
		DoubleNode *newNode = new DoubleNode{};
		newNode->data = data;
		newNode->next = list.front;
		list.front = newNode;
	}

	//insertion at back `insertBack`.
	void insertBack( DoubleLL &list, double data )
	{
		DoubleNode *current = list.front;
		if ( isEmpty(list) )
		{
			insertFront( list, data );
		}
		else
		{
			while ( current->next != nullptr )
			{
				current = current->next;
			}
			current->next = new DoubleNode{};
			current->next->data = data;
			current->next->next = nullptr;
		}
	}

	//remove front `removeFront`.
	void removeFront( DoubleLL &list )
	{
		if ( !isEmpty(list) )
		{
			DoubleNode *current = list.front;
			list.front = list.front->next;
			delete current;
		}
	}

	//remove back `removeBack`.
	void removeBack( DoubleLL &list )
	{
		if ( !isEmpty(list) )
		{
			DoubleNode *current = list.front;
			if ( list.front->next == nullptr )
			{
				list.front = nullptr;
				delete current;
			}
			else
			{
				DoubleNode *previous = nullptr;
				while ( current->next != nullptr )
				{
					previous = current;
					current = current->next;
				}
				previous->next = nullptr;
				delete current;
			}
		}
	}

	//return front `front`.
	double front( const DoubleLL &list )
	{
		if ( !isEmpty(list) )
		{
			return list.front->data;
		}
	}

	//return back `back`.
	double back( const DoubleLL &list )
	{
		if ( !isEmpty(list) )
		{
			DoubleNode *current = list.front;
			while ( current->next != nullptr )
			{
				current = current->next;
			}
			return current->data;
		}
	}

	//get arbitrary kth-element `getAt`.
	double getAt( const DoubleLL &list, int index )
	{
		if ( (!isEmpty(list)) && (index > 0) )
		{
			DoubleNode *current = list.front;
			for ( int i = 1; i != index; ++i )
			{
				current = current->next;
			}
			return current->data;
		}
	}

	//remove the kth-element `removeAt`.
	void removeAt( DoubleLL &list, int index )
	{
		if ( !isEmpty(list) && (index > 0) )
		{
			if ( index == 1 )
			{
				removeFront( list );
			}
			else
			{
				DoubleNode *current = list.front->next;
				DoubleNode *previous = list.front;
				for ( int i = 2; i < index; ++i )
				{
					previous = current;
					current = current->next;
				}
				previous->next = current->next;
				delete current;
			}
		}
	}

	//remove a node next to a given node `removeAfter`.
	void removeAfter( DoubleLL &list, int index )
	{
		removeAt( list, index + 1 );
	}
	
	//remove nodes with given data `filter`.
	void filter( DoubleLL &list, double data )
	{
		while (list.front->data == data)
		{
			removeFront( list );
		}

		DoubleNode *current = list.front->next;
		DoubleNode *catcher = nullptr;
		int i = 2;  //counter will be used as index in 'removeAt' function 
		            //started counting from list.front->next
		while ( current != nullptr )
		{
			if ( current->data == data )
			{
				catcher = current->next;
				removeAt( list , i ); 
				current = catcher;
			}
			else
			{
				current = current->next;
			}
			++i;
		}
	}
	
	//size of the linked list `size`.
	int size( const DoubleLL &list )
	{
		int i = 0; //counter
		DoubleNode *current = list.front;
		while ( current != nullptr )
		{
			++i;
			current = current->next;
		}
		return i;
	}

	//printAll `printAll`.
	void printAll( const DoubleLL &list )
	{
		DoubleNode *current = list.front;
		while ( current != nullptr )
		{
			std::cout << current->data << " ";
			current = current->next;
		}
		std::cout << std::endl;
	}

	//delete the whole list from the heap `clear`.
	void clear( DoubleLL &list )
	{
		DoubleNode *current = list.front;
		DoubleNode *catcher;
		while ( current != nullptr )
		{
			catcher = current->next;
			delete current;
			current = catcher;
		}
	}
}

#endif // MEMBER4_HPP
