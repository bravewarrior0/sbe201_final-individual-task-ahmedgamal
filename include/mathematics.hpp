#ifndef MATHEMATICS_HPP
#define MATHEMATICS_HPP

#include <cmath> // for std::sqrt

namespace mathematics
{

double calculation(double a, double b, char operation)
{
    switch (operation)
    {
    case '+':
    {
        return a + b;
    }
    break;

    case '-':
    {
        return a - b;
    }
    break;

    case '*':
    {
        return a * b;
    }
    break;

    case '/':
    {
        return a / b;
    }
    break;

    default:
    {
        return 0;
    }
    break;
    }
}
struct Triangle
{
    double a;
    double b;
    double c;
};
double heron(double a, double b, double c)
{
    double s = (a + b + c) / 2;
    return sqrt(s * (s - a) * (s - b) * (s - c));
}
double heron(Triangle t)
{
    return heron(t.a , t.b, t.c);  
}
double square(double a) // i created it just for you although I know How to square function using cmath
{
    return a * a;
}
}

#endif // MATHEMATICS_HPP

