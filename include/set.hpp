//
// Created by asem on 01/04/18.
//

#ifndef SBE201_WORDCOUNT_MAPS_SET_HPP
#define SBE201_WORDCOUNT_MAPS_SET_HPP

#include <string>
#include "bst.hpp"

namespace set
{

using WordSet = bst::BSTNode *;


WordSet create()
{
    return nullptr;
}

bool isEmptyS( WordSet &wset )
{
	return wset == nullptr;
}

int sizeS( WordSet wset )
{
	return bst::size( wset );
}

bool contains( WordSet wset , std::string item )
{
	return bst::find( wset, item );
}

void removeS( WordSet &wset , std::string to_remove )
{
	bst::remove( wset, to_remove );
}

void insertS( WordSet &wset , std::string new_item )
{
    if( ! contains( wset , new_item ) )
        bst::insert( wset , new_item );
}

void printAll( WordSet wset )
{
    if( wset )
    {
        printAll( wset->left );
        std::cout << wset->item << std::endl;
        printAll( wset->right );
    }
}

void iterateCopyAll( WordSet &wset, WordSet &host )
{
	if ( wset )
	{
		insertS( host, wset->item );
		iterateCopyAll( wset->left, host );
		iterateCopyAll( wset->right, host );
	}
}

WordSet union_( WordSet &set1 , WordSet &set2 )
{
	WordSet set3 = create();
	iterateCopyAll( set1, set3 );
	iterateCopyAll( set2, set3 );
}

void iterateCopyIntersect( WordSet &wset1, WordSet &wset2, WordSet &host )
{
	if ( wset1 )
	{
		if ( contains(wset2, wset1->item) )
		{
			insertS( host, wset1->item );
			iterateCopyIntersect( wset1->left, wset2, host );
			iterateCopyIntersect( wset1->right, wset2, host );
		}
	}
}

WordSet intersect( WordSet &set1 , WordSet &set2 )
{
	WordSet set3 = create();
	iterateCopyIntersect( set1, set2, set3 );
}

bool iterateCheck( WordSet wset1, WordSet wset2 )
{
	if ( isEmptyS(wset1) != isEmptyS(wset2) )
		return false;
	else
	{
		if ( wset1 && wset2 )
		{
			iterateCheck( wset1->left, wset2->left );
			if (wset1->item.compare(wset2->item) == 0)
			{
				iterateCheck( wset1->right, wset2->right );
				return true;
			}
			else
				return false;
		}
		else return true;
	}
}

bool equals( WordSet &set1 , WordSet &set2 )
{
	bool eqSize( sizeS(set1) == sizeS(set2) );
	return eqSize && iterateCheck(set1,set2);
}

}
#endif //SBE201_WORDCOUNT_MAPS_SET_HPP
