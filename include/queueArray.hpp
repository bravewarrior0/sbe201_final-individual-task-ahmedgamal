/*
 * Requirement 1: (Deadline Saturday 11:59 PM)
    - Implement queues of 'char' and 'double' using arrays and support the operations stated in Assignment 4
    - Make sure to follow the rules of Bonus Grades

 * Requirement 2:
    - Cooperate with Sohail, Osama, Motair and Nasr to finish A3 (You are the team leader)
    - Make sure to follow the rules of Bonus Grades

 * Requirement 3:
    - Fulfill const-correctness for all header files
*/

#ifndef MEMBER1_HPP
#define MEMBER1_HPP
#include <iostream>
#define queueSize 100 

namespace queue
{
struct DoubleQueue
{
    double buffer[queueSize];
    int front = -1;
    int back = -1;
};
bool isEmpty(const DoubleQueue queue)
{
    return (queue.front == -1 && queue.back == -1);
}
bool isFull(const DoubleQueue queue)
{
    return ((queue.back + 1) % queueSize == queue.front);
}
void enqueue(DoubleQueue &queue, double newElement)
{
    if (isFull(queue))
    {
        std::cout << "queue is full" << std::endl;
    }
    else
    {
        if (isEmpty(queue))
            queue.front = 0;

        queue.back = (queue.back + 1) % queueSize;
        queue.buffer[queue.back] = newElement;
    }
}
void dequeue(DoubleQueue &queue)
{
    if (isEmpty(queue))
    {
        std::cout << "queue is empty" << std::endl;
    }
    else
    {
        if (queue.front == queue.back)
            queue.front = queue.back = -1;
        else
            queue.front = (queue.front + 1) % queueSize;
    }
}
double front(const DoubleQueue queue)
{
    if (isEmpty(queue))
        std::cout << "queue is empty" << std::endl;
    else
        return queue.buffer[queue.front];
}
void clear(DoubleQueue &queue)
{
    queue.back = queue.front = -1;
}
int size(const DoubleQueue queue)
{
    if (queue.front <= queue.back)
        return (queue.back - queue.front + 1);
    else
        return (queueSize - queue.front + queue.back + 1);
}
void printQueue(const DoubleQueue queue)
{
    if (isEmpty(queue))
        std::cout << "Queue is empty\n";
    else
    {
        if (queue.back >= queue.front)
        {
            for (int i = queue.front; i <= queue.back; i++)
                std::cout << queue.buffer[i] << " ";
            std::cout << "\n";
        }
        else
        {
            for (int i = queue.front; i < queueSize; i++)
                std::cout << queue.buffer[i] << " ";

            for (int i = 0; i <= queue.back; i++)
                std::cout << queue.buffer[i] << " ";
            std::cout << "\n";
        }
    }
}

struct CharQueue
{
    char buffer[queueSize];
    int front = -1;
    int back = -1;
};
bool isEmpty(const CharQueue queue)
{
    return (queue.front == -1 && queue.back == -1);
}
bool isFull(const CharQueue queue)
{
    return ((queue.back + 1) % queueSize == queue.front);
}
void enqueue(CharQueue &queue, char newElement)
{
    if (isFull(queue))
    {
        std::cout << "queue is full" << std::endl;
    }
    else
    {
        if (isEmpty(queue))
            queue.front = 0;

        queue.back = (queue.back + 1) % queueSize;
        queue.buffer[queue.back] = newElement;
    }
}
void dequeue(CharQueue &queue)
{
    if (isEmpty(queue))
    {
        std::cout << "queue is empty" << std::endl;
    }
    else
    {
        if (queue.front == queue.back)
            queue.front = queue.back = -1;
        else
            queue.front = (queue.front + 1) % queueSize;
    }
}
char front(const CharQueue queue)
{
    if (isEmpty(queue))
        std::cout << "queue is empty" << std::endl;
    else
        return queue.buffer[queue.front];
}
void clear(CharQueue &queue)
{
    queue.back = queue.front = -1;
}
int size(const CharQueue queue)
{
    if (queue.front <= queue.back)
        return (queue.back - queue.front + 1);
    else
        return (queueSize - queue.front + queue.back + 1);
}
void printQueue(const CharQueue queue)
{
    if (isEmpty(queue))
        std::cout << "Queue is empty\n";
    else
    {
        if (queue.back >= queue.front)
        {
            for (int i = queue.front; i <= queue.back; i++)
                std::cout << queue.buffer[i] << " ";
            std::cout << "\n";
        }
        else
        {
            for (int i = queue.front; i < queueSize; i++)
                std::cout << queue.buffer[i] << " ";

            for (int i = 0; i <= queue.back; i++)
                std::cout << queue.buffer[i] << " ";
            std::cout << "\n";
        }
    }
}
}
/*------------------------------------------------------------------------------------*/


#endif // MEMBER1_HPP
